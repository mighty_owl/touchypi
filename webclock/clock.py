from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template("base.html")


@app.route('/alarm')
@app.route('/alarm/<bela>')
def alarm(bela=None):
    return render_template("set_alarm.html", code=bela)


if __name__ == '__main__':
    app.run()
